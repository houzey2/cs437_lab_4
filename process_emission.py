import json
import logging
import sys
import csv
import greengrasssdk
from io import StringIO

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

max_co2 = {}
topic_formatter = "MaxCO2/{}"
def lambda_handler(event, context):
    global max_co2
    # Get your data
    pre_dict = max_co2
    idx = event['index']
    co2 = float(event['co2']) 
    count = event['sequence']
    # Calculate max CO2 emission
    if idx in max_co2:
        max_co2[idx] = max(co2, max_co2[idx])
    else:
        max_co2[idx] = co2

    # Return the result
    # result_message = f"From car {idx}, the max CO2 emission value is {max_co2[idx]}. Invocation Count: {count}."
    topic_name = topic_formatter.format(idx)
    client.publish(
        topic=topic_name,
        payload=json.dumps(
            {"idx": idx, "max_co2": max_co2[idx], "row_count": count}
        ),
    )
    return
