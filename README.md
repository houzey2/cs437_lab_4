# CS437_Lab_4



## File Explaination
1. lab4_emulator_client_updated.py is the python file modified based on the given python file in lab instruction. It is used to test the connection between Greengrass core and the devices we created in part 1.
2. process_emmission.py is the lambda function used on the AWS. It is used to calculate the maximum CO2 sent from devices to Greengrass core.
3. carDiscovery.py is the python file modified based on the basicDiscovery.py that downloaded from official tutoiral from AWS. It is used to publish CO2 to Greengrass core and receive the feedback from the lambda function. It will also store the responed data as csv and save locally.
4. Step_3.ipynb is the jupyter notebook that read responed data from csv files in csv.zip. It will create three different plots to analysis the date. 